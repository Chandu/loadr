﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using Dapper;

namespace Loadr
{
	public class Processor
	{
		public PairProvider PairProvider { get; set; }

		public Processor(PairProvider pairProvider)
		{
			PairProvider = pairProvider;
		}

		public void Load()
		{
			using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TargetDB"].ConnectionString))
			using (var scope = new TransactionScope())
			{
				connection.Open();
				connection.Execute(@"
					INSERT NumericPairs(First, Second)
					VALUES(@First, @Second)", 
					PairProvider.GetPairs());
				scope.Complete();
				connection.Close();
			}
		}
	}

	/*
	  CREATE TABLE NumericPairs
		(
			First NUMERIC(18,8),
			Second NUMERIC(18,8)
		)
	 */
	public class NumericPair
	{
		public double First { get; set; }
		public double Second { get; set; }
	}

	public class PairProvider
	{
		public string FilePath { get; set; }

		public PairProvider(string filePath)
		{
			FilePath = filePath;
		}

		public IEnumerable<NumericPair> GetPairs()
		{
			var lines = System.IO.File.ReadLines(FilePath);
			return
				lines.SelectMany(line => line.Split(new[] { '|' })
					.Where(a => a.Trim().Length > 0)
					.Select(a =>
					{
						var pair = a.Split(new[] {' '});
						return new NumericPair
						{
							First = Convert.ToDouble(pair[0]),
							Second = Convert.ToDouble(pair[1])
						};
					}));

			
		}
	}
}
